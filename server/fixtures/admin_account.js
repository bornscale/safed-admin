const ROLES = ['god', 'admin', 'editor', 'viewer'];
Meteor.startup(function() {
  //
  //  Add root
  //
  if (!Meteor.users.findOne({
      'emails.address': 'admin@admin.com'
    })) {
    let userId = Accounts.createUser({
      email: 'admin@admin.com',
      password: 'q1w2e3',
      profile: {
        firstName: 'Root',
        lastName: 'User',
        fullName: 'Root User',
        cpf: '416.491.798-98',
        rg: '35.854.104-9',
        localizacao: {
          cep: '04726-240',
          endereco: {
            rua: 'R. localhost',
            numero: '1337'
          }
        },
        contato: {
          telefone: '(11) 94343-3330',
          celular: '(11) 94343-3330',
          email: 'rafael.correia.poli@gmail.com'
        }
      }
    });
    Roles.addUsersToRoles(userId, 'admin');
  }

  //
  //  Preserve only determined ROLES, add them if do not exist
  //
    Meteor.roles.remove({
      name: {$nin: ROLES}
    });
  _.each(ROLES, function(role, index) {
    let existingRole = Meteor.roles.findOne({name: role});
    Meteor.roles.upsert({
      name: role
    }, {
      name: role,
      hierarchy: index
    });
  });
});
