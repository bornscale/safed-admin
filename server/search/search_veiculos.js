SearchSource.defineSource('veiculos', function(searchText, options) {
  if (searchText) {
    let regExp = buildRegExp(searchText);
    let selector = {
      $or: [{
        placa: regExp
      }, {
        modelo: regExp
      }]
    };
    return Veiculos.find(selector, options).fetch();
  }
  return Veiculos.find({}, options).fetch();
});
