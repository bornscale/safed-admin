SearchSource.defineSource('motoristas', function(searchText, options) {
  if (searchText) {
    let regExp = buildRegExp(searchText);
    let selector = {
      $or: [{
        nome: regExp
      }, {
        'cnh.numero': regExp
      }]
    };
    return Motoristas.find(selector, options).fetch();
  }
  return Motoristas.find({}, options).fetch();
});
