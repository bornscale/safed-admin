SearchSource.defineSource('autuacoes', function(searchText, options) {
  if (searchText) {
    let regExp = buildRegExp(searchText);
    let selector = {
      $or: [{
        infracao: regExp
      }, {
        enquadramento: regExp
      }, {
        notificacao: regExp
      }]
    };
    return Autuacoes.find(selector, options).fetch();
  }
  return Autuacoes.find({}, options).fetch();
});
