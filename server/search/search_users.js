SearchSource.defineSource('users', function(searchText, options) {
  if (searchText) {
    let regExp = buildRegExp(searchText);
    let selector = {
      $or: [{
        'profile.fullName': regExp,
      }, {
        'profile.rg': regExp
      }, {
        'profile.cpf': regExp
      }, {
        username: regExp
      }, {
        'profile.contato.email': regExp
      }]
    };
    return Meteor.users.find(selector, options).fetch();
  }
  return Meteor.users.find({}, options).fetch();
});
