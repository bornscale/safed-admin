SearchSource.defineSource('sinistros', function(searchText, options) {
  if (searchText) {
    let regExp = buildRegExp(searchText);
    let selector = {
      $or: [{
        'BO.numero': regExp,
      }, {
        'condutor.nome': regExp
      }]
    };
    return Sinistros.find(selector, options).fetch();
  }
  return Sinistros.find({}, options).fetch();
});
