SearchSource.defineSource('empresas', function(searchText, options) {
  if (searchText) {
    let regExp = buildRegExp(searchText);
    let selector = {
      $or: [{
        razao: regExp
      }, {
        cnpj: regExp
      }]
    };
    return Empresas.find(selector, options).fetch();
  }
  return Empresas.find({}, options).fetch();
});
