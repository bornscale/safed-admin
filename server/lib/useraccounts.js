Accounts.urls.enrollAccount = function(token) {
  return Meteor.absoluteUrl('enroll-account/' + token);
};

Accounts.urls.resetPassword = function(token) {
  return Meteor.absoluteUrl('reset-password/' + token);
};

Accounts.onCreateUser(function(options, user) {
  user.profile = options.profile || {};
  return user;
});
