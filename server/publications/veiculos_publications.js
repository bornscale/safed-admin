Meteor.publishComposite('veiculos.single', function(veiculoId) {
  check(veiculoId, String);
  this.unblock();

  return {
    find() {
      this.unblock();
      return Veiculos.find({
        _id: veiculoId
      });
    }
  };
});
