Meteor.publishComposite('autuacoes.single', function(autuacaoId) {
  check(autuacaoId, String);
  this.unblock();

  return {
    find() {
      this.unblock();
      return Autuacoes.find({
        _id: autuacaoId
      });
    }
  };
});
