Meteor.publishComposite('empresas.single', function(empresaId) {
  check(empresaId, String);
  this.unblock();

  return {
    find() {
      this.unblock();
      return Empresas.find({
        _id: empresaId
      });
    }
  };
});


Meteor.publishComposite('empresas', function(searchFields) {
  check(searchFields, Object);
  let filterSchema = Schemas.Empresa.pick(['razao', 'cnpj']);
  filterSchema.clean(searchFields);
  this.unblock();

  let query = {
    razao: new RegExp(searchFields.razao, 'i'),
    cnpj: new RegExp(searchFields.cnpj, 'i')
  };

  return {
    find() {
      return Empresas.find(query, {
        sort: {
          razao: 1
        }
      });
    }
  };
});

