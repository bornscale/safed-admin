Meteor.publish('docCounter', function() {
  Counts.publish(this, 'docCounter-motoristas', Motoristas.find());
  Counts.publish(this, 'docCounter-veiculos', Veiculos.find());
  Counts.publish(this, 'docCounter-autuacoes', Autuacoes.find());
  Counts.publish(this, 'docCounter-sinistros', Sinistros.find());
});
