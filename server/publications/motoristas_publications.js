Meteor.publishComposite('motoristas.single', function(motoristaId) {
  check(motoristaId, String);
  this.unblock();

  return {
    find() {
      this.unblock();
      return Motoristas.find({
        _id: motoristaId
      });
    }
  };
});
