Meteor.publishComposite('sinistros.single', function(sinistroId) {
  check(sinistroId, String);
  this.unblock();

  return {
    find() {
      this.unblock();
      return Sinistros.find({
        _id: sinistroId
      });
    }
  };
});
