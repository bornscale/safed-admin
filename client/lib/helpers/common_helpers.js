Template.registerHelper('SearchSources', () => SearchSources);
Template.registerHelper('Schemas', () => Schemas);


Template.registerHelper('getGenderIcon', (gender) => {
  let icon = gender && Icons.gender[gender.toLowerCase()] || Icons.gender.default;
  return `<i class="${icon}""></i>`;
});

Template.registerHelper('formatOrgType', function(orgType, makeBadge) {
  let text = capitalizeFirstLetter(orgType);
  let color = orgType && Resources.Colors.org.type[orgType.toLowerCase()] || Resources.Colors.org.type.default;
  return makeBadge ? `<span class="badge badge-${color}">${text}</span>` : text;
});

Template.registerHelper('formatRole', function(role, makeBadge) {
  let text = capitalizeFirstLetter(role);
  let color = role && Resources.Colors.users.role[role.toLowerCase()] || Resources.Colors.users.role.default;
  return makeBadge ? `<span class="badge badge-${color}">${text}</span>` : text;
});

Template.registerHelper('formatReais', function(valor) {
  return accounting.formatMoney(valor, 'R$ ', 2, '.', ',');
});

Template.registerHelper('capitalizeFirstLetter', (text) => {
  return capitalizeFirstLetter(text);
});

Template.registerHelper('shortIt', function(stringToShorten, maxCharsAmount) {
  if (stringToShorten.length > maxCharsAmount) {
    return stringToShorten.substring(0, maxCharsAmount) + '...';
  }
  return stringToShorten;
});