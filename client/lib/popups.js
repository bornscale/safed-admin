removeConfirmation = function(name, callback) {
  swal({
    title: 'Tem certeza?',
    text: 'Você não poderá recuperar o documento <b>"' + name + '"</b>',
    html: true,
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Sim',
    closeOnConfirm: true
  }, () => {
    callback();
  });
};

removeSuccess = function() {
  swal('Deleted!', 'O documento foi deletado.', 'success');
};

removeError = function() {
  swal('Error!', 'O documento não foi deletado.', 'danger');
};


handleTableClick = function(event, callback) {
    let dataTable = $(event.target).closest('table').DataTable();
    let rowData = dataTable.row(event.currentTarget).data();
    if (!rowData) return; // Won't be data if a placeholder row is clicked
    callback(rowData);
};
