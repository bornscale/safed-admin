Colors = {
  org: {
    type: {
      academic: 'info',
      'non-profit': 'success',
      governamental: 'danger',
      private: 'warning',
      default: 'primary'
    }
  },
  users: {
    role: {
      admin: 'info',
      god: 'danger',
      viewer: 'primary',
      editor: 'warning',
      default: 'default'
    }
  },
  operations: {
    insert: 'info',
    update: 'success',
    remove: 'danger',
    default: 'default'
  }
};
