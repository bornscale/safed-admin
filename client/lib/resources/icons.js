Icons = {
  menu: {
    dashboard: 'fa fa-dashboard',
    home: 'fa fa-home'
  },
  roles: {
    god: 'fa fa-star',
    admin: 'fa fa-wrench',
    viewer: 'fa fa-eye',
    editor: 'fa fa-pencil-square-o'
  },
  misc: {
    updates: 'fa fa-bell',
    user: 'fa fa-user'
  },
  collections: {
    users: 'fa fa-users',
    veiculos: 'fa fa-car',
    sinistros: 'fa fa-list-alt',
    empresas: 'fa fa-building-o',
    motoristas: 'fa fa-user',
    autuacoes: 'fa fa-hand-paper-o',
    default: 'fa fa-question'
  },
  gender: {
    male: 'fa fa-mars',
    female: 'fa fa-venus'
  }
};

