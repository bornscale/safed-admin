Resources = {
  Icons: Icons,
  Countries: Countries,
  Colors: Colors
};

Template.registerHelper('Resources', () => Resources);
