T9n.setLanguage("pt");
T9n.map('pt', {
        error: {
            accounts: {
                'Login forbidden': 'E-mail ou senha incorretos'
            }
        }
    });
AccountsTemplates.configure({
  confirmPassword: true,
  overrideLoginErrors: true,
  texts: {
    title: {
      signIn: 'Faça seu Login',
      signUp: 'Crie sua conta',
      forgotPwd: 'E-mail associado a conta',      
    },
    button: {
      signIn: 'Entrar',
      signUp: 'Criar minha Conta',
      forgotPwd: 'Enviar link para redefinição',
    },
    errors:{
      mustBeLoggedIn: 'Precisa estar logado!',
      pwdMismatch: 'Senhas não correspondem',
      loginForbidden: 'E-mail ou senha incorretos',
      validationErrors: 'Erro na validação',
    },
    signInLink_pre: 'Já possui uma conta?',
    signInLink_link: 'Faça o Login',
    signUpLink_pre: 'Ainda não criou sua conta?',
    signUpLink_link: 'Clique aqui',
    socialSignIn: 'Login',
    socialSignUp: 'Cadastrar',
    pwdLink_link: 'Esqueceu a senha?',
    requiredField: 'Campo obrigatório',
  },   
});

AccountsTemplates.removeField('email');
AccountsTemplates.addFields([
  {                                                                                                  
    _id: "email",                                                                                              
    type: "email",                                                                                            
    required: true,                                                                                            
    lowercase: true,                                                                                           
    trim: true,                                                                                                
    func: function(email) {                                                                                    
        return !_.contains(email, '@');                                                                        
    },                                                                                                         
    errStr: 'Email invalido',                                                                                   
  }
])