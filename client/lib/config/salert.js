Meteor.startup(function() {
  sAlert.config({
    effect: 'stackslide',
    position: 'top',
    timeout: 5000,
    html: true,
    onRouteClose: false,
    offset: 0,
  });
});
