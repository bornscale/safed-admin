Template.mainDashboard.helpers({
  motoristasCount() {
    return Counts.get('docCounter-motoristas');
  },
  veiculosCount() {
    return Counts.get('docCounter-veiculos');
  },
  autuacoesCount() {
    return Counts.get('docCounter-autuacoes');
  },
  sinistrosCount() {
    return Counts.get('docCounter-sinistros');
  }
});

Template.mainDashboard.onCreated(function() {
  this.subscribe('docCounter');
});
