Template.userCard.events({
  'click .delete': function(e, t) {
    e.preventDefault();
    console.log(this);
    removeConfirmation(this.profile && this.profile.fullName, () => {
      Meteor.call('Users.methods.remove', this._id, (err, res) => {
        if (err) {
          removeError();
        }
        removeSuccess();
        t.isDeleted.set(true);
      });
    });
  },
});

Template.userCard.onCreated(function() {
  this.isDeleted = new ReactiveVar(false);
  this.subscribe('user.status', this.data._id);

  console.log('on Created!');
  this.autorun(() => {
    let cData = Template.currentData();
    console.log(cData);
    if (cData.profile && cData.profile.imageId) {
      console.log('subscribing to ', cData.profile.imageId)
      this.subscribe('images.single', cData.profile.imageId);
    }
  });  
});

Template.userCard.helpers({
  user() {
    return Meteor.users.findOne({
      _id: Template.instance().data._id
    });
  },
  isDeleted() {
    return Template.instance().isDeleted.get();
  }
});
