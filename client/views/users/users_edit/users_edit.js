Template.usersEdit.helpers({
  user() {
    return Meteor.users.findOne({
      _id: FlowRouter.getParam('id')
    });
  }
});

AutoForm.hooks({
  updateUsersForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Usuário editado com sucesso');
      FlowRouter.go('users.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});
