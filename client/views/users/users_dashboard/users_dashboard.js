Template.usersDashboard.helpers({
  users() {
    return SearchSources.users.getTransformedData();
  }
});
