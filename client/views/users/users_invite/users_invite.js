AutoForm.hooks({
  inviteUsersForm: {

    // Called when any submit operation succeeds
    onSuccess: function(formType, result) {
      sAlert.success('Usuário convidado com sucesso: ' + this.insertDoc.email);
      FlowRouter.go('users.dashboard');
    },

    // Called when any submit operation fails
    onError: function(formType, error) {
      console.log(error);
      sAlert.error(error.toString());
    },
  }
});
