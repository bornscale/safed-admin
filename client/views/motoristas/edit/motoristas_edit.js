AutoForm.hooks({
  updateMotoristasForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Motorista editado com sucesso');
      FlowRouter.go('motoristas.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});

Template.motoristasEdit.onCreated(function() {
  this.subscribe('motoristas.single', FlowRouter.getParam('id'));
});

Template.motoristasEdit.helpers({
  tech() {
    return Motoristas.findOne(FlowRouter.getParam('id'));
  }
});
