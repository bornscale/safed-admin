Template.motoristasCard.onCreated(function() {
  this.deleted = new ReactiveVar();

  this.autorun(() => {
    let cData = Template.currentData();
    if (cData.foto) {
      this.subscribe('images.single', cData.foto);
    }

    if (cData.cnh.imagem) {
      this.subscribe('images.single', cData.cnh.imagem);
    }
  });
});

Template.motoristasCard.helpers({
  isDeleted() {
    return Template.instance().deleted.get();
  }
});


Template.motoristasCard.events({
  'click .delete': function(e, t) {
    removeConfirmation(this.nome, () => {
      Meteor.call('Motoristas.methods.remove', this._id, (err, res) => {
        if (err) return removeError();
        sAlert.success('<i class="fa fa-check"></i> Motorista removido com sucesso.');
        t.deleted.set(true);
      });
    });
  },
  'click .modalDocumento': function() {
    Modal.show('modalDocumento', {
      imagemId: this.cnh.imagem
    });
  }
});
