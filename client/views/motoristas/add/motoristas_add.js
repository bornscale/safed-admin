AutoForm.hooks({
  insertMotoristasForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Motorista criado com sucesso');
      FlowRouter.go('motoristas.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});
