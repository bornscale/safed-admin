AutoForm.hooks({
  insertAutuacoesForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Autuação criado com sucesso');
      FlowRouter.go('autuacoes.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});
