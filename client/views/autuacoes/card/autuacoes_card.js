Template.autuacoesCard.onCreated(function() {
  this.deleted = new ReactiveVar();

  this.autorun(() => {
    let cData = Template.currentData();
    if (cData.imagem) {
      this.subscribe('images.single', cData.imagem);
    }
  });
});

Template.autuacoesCard.helpers({
  isDeleted() {
    return Template.instance().deleted.get();
  }
});

Template.autuacoesCard.events({
  'click .delete': function(e, t) {
    removeConfirmation(this.notificacao, () => {
      Meteor.call('Autuacoes.methods.remove', this._id, (err, res) => {
        if (err) return removeError();
        sAlert.success('<i class="fa fa-check"></i> Autuação removida com sucesso.');
        t.deleted.set(true);
      });
    });
  },
  'click .modalDocumento': function() {
    Modal.show('modalDocumento', {
      imagemId: this.imagem
    });
  }
});
