AutoForm.hooks({
  updateAutuacoesForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Autuação editada com sucesso');
      FlowRouter.go('autuacoes.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});

Template.autuacoesEdit.onCreated(function() {
  this.subscribe('autuacoes.single', FlowRouter.getParam('id'));
});

Template.autuacoesEdit.helpers({
  autuacao() {
    return Autuacoes.findOne(FlowRouter.getParam('id'));
  }
});
