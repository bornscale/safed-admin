Template.filterForm.helpers({
  filterChange() {
    return Template.instance().data.filterChange;
  }
});

Template.filterForm.onRendered(function() {
  AutoForm.addHooks([this.data.formId], {
    onSubmit: function(insertDoc, updateDoc, currentDoc) {
      return false;
    },
    formToDoc(doc) {
      let templateData = this.template.data || {};
      if (templateData.filterChange && typeof templateData.filterChange === 'function') {
        templateData.filterChange(doc);
      }
    }
  }, true);
});
