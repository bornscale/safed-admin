Template.navigation.events({
  'click #logout': function() {
    AccountsTemplates.logout();
  }
});

Template.navigation.onRendered(function() {
  // Initialize metisMenu
  $('#side-menu').metisMenu();
});

// Used only on OffCanvas layout
Template.navigation.events({
  'click .close-canvas-menu': function() {
    $('body').toggleClass('mini-navbar');
  }
});


/*Template.navigation.onCreated(function() {
  this.autorun(() => {
    if (Meteor.user() && Meteor.user().profile && Meteor.user().profile.imageId) {
      //  this.subscribe('images.single', Meteor.user().profile.imageId);
    }
  });    
});*/