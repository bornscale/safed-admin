const ADMIN = ['admin'];

Template.menu.helpers({
  items: function() {
    return [{
      regex: '^main.dashboard',
      name: 'Home',
      icon: Resources.Icons.menu.home,
      roles: ADMIN,
      submenu: [{
        name: 'Dashboard',
        linkTo: 'main.dashboard',
        roles: ADMIN
      }]
    }, {
      regex: '^users',
      name: 'Usuários',
      icon: Resources.Icons.collections.users,
      roles: ADMIN,
      submenu: [{
        name: 'Gerenciar',
        linkTo: 'users.dashboard',
        roles: ADMIN
      }, {
        roles: ADMIN,
        linkTo: 'users.invite',
        name: 'Convidar Usuário'
      }]
    }, {
      regex: '^motoristas',
      name: 'Motoristas',
      icon: Resources.Icons.collections.motoristas,
      roles: ADMIN,
      submenu: [{
        roles: ADMIN,
        linkTo: 'motoristas.dashboard',
        name: 'Gerenciar'
      }, {
        roles: ADMIN,
        linkTo: 'motoristas.add',
        name: 'Criar'
      }]
    }, {
      regex: '^veiculos',
      name: 'Veículos',
      icon: Resources.Icons.collections.veiculos,
      roles: ADMIN,
      submenu: [{
        roles: ADMIN,
        linkTo: 'veiculos.dashboard',
        name: 'Gerenciar'
      }, {
        roles: ADMIN,
        linkTo: 'veiculos.add',
        name: 'Criar'
      }]
    }, {
      regex: '^empresas',
      name: 'Empresas',
      icon: Resources.Icons.collections.empresas,
      roles: ADMIN,
      submenu: [{
        roles: ADMIN,
        linkTo: 'empresas.dashboard',
        name: 'Gerenciar'
      }, {
        roles: ADMIN,
        linkTo: 'empresas.add',
        name: 'Criar'
      }]
    }, {
      regex: '^autuacoes',
      name: 'Autuações',
      icon: Icons.collections.autuacoes,
      roles: ADMIN,
      submenu: [{
        roles: ADMIN,
        linkTo: 'autuacoes.dashboard',
        name: 'Gerenciar'
      }, {
        roles: ADMIN,
        linkTo: 'autuacoes.add',
        name: 'Criar'
      }]
    }, {
      regex: '^sinistros',
      name: 'Sinistros',
      icon: Icons.collections.sinistros,
      roles: ADMIN,
      submenu: [{
        roles: ADMIN,
        linkTo: 'sinistros.dashboard',
        name: 'Gerenciar'
      }, {
        roles: ADMIN,
        linkTo: 'sinistros.add',
        name: 'Criar'
      }]
    }];
  }
});
