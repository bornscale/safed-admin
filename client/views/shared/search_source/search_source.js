SearchSources = {};
const SEARCH_THROTTLE = 200;
const SEARCH_OPTIONS = {
  keepHistory: false,
  localSearch: false
};
SearchSources.globalSearch = new SearchSource('globalSearch',
  ['name', 'description'],
  SEARCH_OPTIONS);

SearchSources.motoristas = new SearchSource('motoristas',
  ['nome', 'cnh.numero'],
  SEARCH_OPTIONS);

SearchSources.empresas = new SearchSource('empresas',
  ['razao', 'cnpj'],
  SEARCH_OPTIONS);

SearchSources.veiculos = new SearchSource('veiculos',
  ['proprietario', 'placa', 'modelo'],
  SEARCH_OPTIONS);

SearchSources.autuacoes = new SearchSource('autuacoes',
  ['infracao', 'enquadramento', 'notificacao'],
  SEARCH_OPTIONS);

SearchSources.sinistros = new SearchSource('sinistros',
  ['BO.numero', 'condutor.nome'],
  SEARCH_OPTIONS);

SearchSources.users = new SearchSource('users',
  ['profile.fullName', 'username', 'profile.cpf', 'profile.rg', 'profile.contato.email'],
  SEARCH_OPTIONS);


SearchSource.prototype.getTransformedData = function() {
  return this.getData({
    transform(matchText, regExp) {
      matchText = String(matchText);

      if (matchText.replace) {
        return matchText.replace(regExp, '<em>$&</em>');
      }
      return matchText;
    },
    sort: {
      _score: -1
    }
  });
};

Template.searchSource.helpers({
  metadata() {
    return Template.instance().source.getMetadata();
  },
  searchStatus() {
    return Template.instance().source.getStatus();
  }
});


Template.searchSource.events({
  'input #search-text': _.throttle(function(e, t) {
    let el = $(e.target);
    let searchText = el.val();
    t.searchText.set(searchText);
  }, 200),
  'click .refresh': function(e, t) {
    t.makeSearch();
  }
});

Template.searchSource.onCreated(function() {
  this.source = this.data.source;
  this.options = this.data.options || {};
  this.searchText = new ReactiveVar('');
  this.makeSearch = function() {
    let opt = typeof this.options === 'function' ? this.options() : this.options;
    this.source.search(this.searchText.get(), opt);
  };

  if (!this.source || !this.source instanceof SearchSource) {
    throw new Error('source must be instance of SearchSource');
  }
  this.autorun(() => {
    this.makeSearch();
  });

  Meteor.setTimeout(() => {
    //  this.makeSearch();
  }, 1000);
});
