AutoForm.addInputType("cep", {
  template: "afInputCep",
  contextAdjust: function(context) {
    if (typeof context.atts.maxlength === "undefined" && typeof context.max === "number") {
      context.atts.maxlength = context.max;
    }
    return context;
  }
});

Template.afInputCep.onRendered(function() {
  var el = this.find("input");
  $(el).mask("99999-999");
});

Template.afInputCep.events({
  'keyup input[name="localizacao.cep"]' (e) {
    let cep = e.target.value;
    if (!cep) return;

    cep = cep.replace(/\D+/g, ''); // Remove symbols
    if (cep.length === 8) {
      $.get(`https://api.postmon.com.br/v1/cep/${cep}`, function(data) {
        const form = $(e.target).closest('form');

        form.find('input[name="localizacao.estado"]').val(data.estado);
        form.find('input[name="localizacao.cidade"]').val(data.cidade);
        form.find('input[name="localizacao.bairro"]').val(data.bairro);
        form.find('input[name="localizacao.endereco.rua"]').val(data.logradouro);
      });
    }
  }
});