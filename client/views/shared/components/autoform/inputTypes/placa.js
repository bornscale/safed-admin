AutoForm.addInputType('placa', {
  template: 'afInputPlaca',
  contextAdjust: function(context) {
    if (typeof context.atts.maxlength === 'undefined' && typeof context.max === 'number') {
      context.atts.maxlength = context.max;
    }
    return context;
  }
});

Template.afInputPlaca.onRendered(function() {
  let el = this.find('input');
  $(el).mask('aaa-9999');
});
