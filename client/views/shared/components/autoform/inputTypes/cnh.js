AutoForm.addInputType('cnh', {
  template: 'afInputCnh',
  contextAdjust: function(context) {
    if (typeof context.atts.maxlength === 'undefined' && typeof context.max === 'number') {
      context.atts.maxlength = context.max;
    }
    return context;
  }
});

Template.afInputCnh.onRendered(function(){
  let el = this.find('input');
  $(el).mask('99999999999');
});
