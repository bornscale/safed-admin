Template.veiculosCard.onCreated(function() {
  this.deleted = new ReactiveVar();

  this.autorun(() => {
    let cData = Template.currentData();
    if (cData.imagem) {
      this.subscribe('images.single', cData.imagem);
    }

    if (cData.documentacao.imagem) {
      this.subscribe('images.single', cData.documentacao.imagem);
    }
  });
});

Template.veiculosCard.helpers({
  isDeleted() {
    return Template.instance().deleted.get();
  }
});

Template.veiculosCard.events({
  'click .delete': function(e, t) {
    removeConfirmation(this.modelo, () => {
      Meteor.call('Veiculos.methods.remove', this._id, (err, res) => {
        if (err) return removeError();
        sAlert.success('<i class="fa fa-check"></i> Veículo removido com sucesso.');
        t.deleted.set(true);
      });
    });
  },
  'click .modalDocumento': function() {
    Modal.show('modalDocumento', {
      imagemId: this.documentacao.imagem
    });
  }
});
