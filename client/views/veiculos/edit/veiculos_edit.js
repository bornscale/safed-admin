AutoForm.hooks({
  updateVeiculosForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Veículo editado com sucesso');
      FlowRouter.go('veiculos.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});

Template.veiculosEdit.onCreated(function() {
  this.subscribe('veiculos.single', FlowRouter.getParam('id'));
});

Template.veiculosEdit.helpers({
  veiculo() {
    return Veiculos.findOne(FlowRouter.getParam('id'));
  }
});
