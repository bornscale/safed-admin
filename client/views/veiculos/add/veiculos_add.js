AutoForm.hooks({
  insertVeiculosForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Veículo criado com sucesso');
      FlowRouter.go('veiculos.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});
