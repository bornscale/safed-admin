AutoForm.hooks({
  insertEmpresasForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Empresa criada com sucesso');
      FlowRouter.go('empresas.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});
