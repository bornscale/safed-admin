Template.empresasCard.events({
  'click .toggle-show': function(e, t) {
    t.showMore.set(!t.showMore.get());
  }
});

Template.empresasCard.helpers({
  deleted() {
    return Template.instance().deleted.get();
  }
});


Template.empresasCard.onCreated(function() {
  this.deleted = new ReactiveVar();

  this.autorun(() => {
    let cData = Template.currentData();

    if (cData.logotipo) {
      this.subscribe('images.single', cData.logotipo);
    }
  });
});


Template.empresasCard.events({
  'click .delete': function(e, t) {
    removeConfirmation(this.razao, () => {
      Meteor.call('Empresas.methods.remove', this._id, (err, res) => {
        if (err) return removeError();
        sAlert.success('<i class="fa fa-check"></i> Empresa removida com sucesso.');
        t.deleted.set(true);
      });
    });
  }
});
