AutoForm.hooks({
  updateEmpresasForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Empresa editada com sucesso');
      FlowRouter.go('empresas.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});

Template.empresasEdit.onCreated(function() {
  this.subscribe('empresas.single', FlowRouter.getParam('id'));
});

Template.empresasEdit.helpers({
  empresa() {
    return Empresas.findOne({
      _id: FlowRouter.getParam('id')
    });
  }
});
