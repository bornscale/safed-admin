/*EmpresasSubs = new SubsManager();

Template.empresasDashboard.helpers({
  empresas() {
    let searchFields = Template.instance().searchFields.get();

    let query = {
      razao: new RegExp(searchFields.razao, 'i'),
      cnpj: new RegExp(searchFields.cnpj, 'i')
    };

    return Empresas.find(query, {
      sort: {
        razao: 1
      }
    });
  },
  filterChange() {
    let template = Template.instance();
    return function(doc) {
      template.searchFields.set(doc);
    };
  },
  filteredFields() {
    return ['razao', 'cnpj'];
  },
  empresasReady() {
    return Template.instance().ready.get();
  }
});

Template.empresasDashboard.onCreated(function() {
  this.searchFields = new ReactiveVar({});
  this.ready = new ReactiveVar();

  this.autorun(() => {
    // this.subscribe('empresas', this.searchFields.get());
    let handle = EmpresasSubs.subscribe('empresas', this.searchFields.get());
    this.ready.set(handle.ready());
  });
});

*/

Template.empresasDashboard.helpers({
  results() {
    return SearchSources.empresas.getTransformedData();
  }
});
