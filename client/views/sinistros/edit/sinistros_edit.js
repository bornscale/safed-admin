AutoForm.hooks({
  updateSinistrosForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Sinistro editado com sucesso');
      FlowRouter.go('sinistros.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});

Template.sinistrosEdit.onCreated(function() {
  this.subscribe('sinistros.single', FlowRouter.getParam('id'));
});

Template.sinistrosEdit.helpers({
  tech() {
    return Sinistros.findOne(FlowRouter.getParam('id'));
  }
});
