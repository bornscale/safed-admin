AutoForm.hooks({
  insertSinistrosForm: {
    onSuccess() {
      sAlert.success('<i class="fa fa-check"></i> Sinistro criado com sucesso');
      FlowRouter.go('sinistros.dashboard');
    },
    onError(formType, error) {
      sAlert.error(error.toString());
    },
  }
});
