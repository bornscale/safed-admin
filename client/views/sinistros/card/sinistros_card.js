Template.sinistrosCard.helpers({
  BOnumero() {
    return this['BO.numero'];
  },
  isDeleted() {
     return Template.instance().deleted.get();
  }
});


Template.sinistrosCard.onCreated(function() {
  this.deleted = new ReactiveVar();
  this.autorun(() => {
    let cData = Template.currentData();
    if (cData.BO && cData.BO.imagem) {
      this.subscribe('images.single', cData.BO.imagem);
    }
  });
});


Template.sinistrosCard.events({
  'click .delete': function(e, t) {
    removeConfirmation(this.BO.numero, () => {
      Meteor.call('Sinistros.methods.remove', this._id, (err, res) => {
        if (err) return removeError();
        sAlert.success('<i class="fa fa-check"></i> Sinistro removido com sucesso.');
        t.deleted.set(true);
      });
    });
  },
  'click .modalDocumento': function() {
    Modal.show('modalDocumento', {
      imagemId: this.BO.imagem
    });
  },
  'click .modalDetalhesSinistro': function() {
    Modal.show('modalDetalhesSinistro', {
      sinistro: this
    });
  }
});
