function createThumb(fileObj, readStream, writeStream) {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name())
    .resize('100', '100', '^')
    .gravity('Center')
    .crop('100', '100')
    .stream().pipe(writeStream);
}

function createCard(fileObj, readStream, writeStream) {
  // Transform the image into a 10x10px thumbnail
  gm(readStream, fileObj.name())
    .resize('600', '400', '^')
    .gravity('Center')
    .crop('600', '400')
    .stream().pipe(writeStream);
}

if (Meteor.isServer) {
  let originalStore = new FS.Store.S3('original', {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    bucket: process.env.AWS_S3_BUCKET,
    region: process.env.AWS_S3_REGION,
    folder: '/original',
    ACL: 'public-read',
  });
  let thumbStore = new FS.Store.S3('thumbs', {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    bucket: process.env.AWS_S3_BUCKET,
    region: process.env.AWS_S3_REGION,
    folder: 'thumbs',
    transformWrite: createThumb
  });
  let cardStore = new FS.Store.S3('cards', {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    bucket: process.env.AWS_S3_BUCKET,
    region: process.env.AWS_S3_REGION,
    folder: 'cards',
    transformWrite: createCard
  });

  Images = new FS.Collection('images', {
    stores: [originalStore, thumbStore, cardStore],
    filter: {
      allow: {
        contentTypes: ['image/*']
      }
    }
  });
}


// On the client just create a generic FS Store as don't have
// access (or want access) to S3 settings on client
if (Meteor.isClient) {
  //let thumbStore = new FS.Store.S3('original');
  let originalStore = new FS.Store.S3('original');

  Images = new FS.Collection('images', {
    stores: [originalStore],
    filter: {
      allow: {
        contentTypes: ['image/*']
      },
      onInvalid: function(message) {
        sAlert.error(message);
      }
    }
  });
}

// Allow rules
Images.allow({
  insert: function() {
    return true;
  },
  update: function() {
    return true;
  },
  remove: function() {
    return true;
  },
  download: function() {
    return true;
  }
});
