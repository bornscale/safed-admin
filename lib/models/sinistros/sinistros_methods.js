Sinistros.methods = {};

Sinistros.methods.add = new ValidatedMethod({
  name: 'Sinistros.methods.add',
  validate: Schemas.Sinistro.validator(),
  run(doc) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Sinistros.insert(doc);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Sinistros.methods.update = new ValidatedMethod({
  name: 'Sinistros.methods.update',
  validate: Schemas.validatedMethodUpdateSchema.validator(),
  run({ _id, modifier }) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Sinistros.update(_id, modifier);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Meteor.methods({
  'Sinistros.methods.remove': function(sinistroId) {
    check(sinistroId, String);
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Sinistros.remove({ _id: sinistroId });
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});
