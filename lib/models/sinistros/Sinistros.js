Sinistros = new Mongo.Collection('sinistros');

/**
 *
 * Schema
 *
 */
Schemas.Sinistro = new SimpleSchema({
  BO: {
    type: Schemas.BO,
    label: 'B.O.'
  },
  condutor: {
    type: Schemas.Condutor
  },
  data: {
    type: Date,
    optional: true,
    autoform: {
      type: 'bootstrap-datepicker',
      datePickerOptions: {
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      }
    }
  },
  // DUVIDA: Guardo apenas Hora ou Hora:Minuto ?
  // TODO: Será um field automático
  hora: {
    type: Date,
    optional: true,
    autoValue() {
      return new Date();
    },
    autoform: {
      omit: true
    }
  },
  local: {
    type: String
  },
  vitimas: {
    type: Number,
    label: 'Vítimas'
  },
  descricao: {
    type: String,
    label: 'Descrição',
    autoform: {
      rows: 6
    }
  },
  terceiros: {
    type: [Schemas.Condutor]
  },
  obs: {
    type: String,
    label: 'Observação',
    autoform: {
      rows: 6
    }
  }
});


/**
 *
 * Helpers
 *
 */
Sinistros.helpers({

});

/**
 *
 * Behaviours
 *
 */
Sinistros.attachSchema(Schemas.Sinistro);
Sinistros.attachBehaviour('timestampable');
