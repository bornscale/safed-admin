Schemas = {};
Meteor.isClient && Template.registerHelper('Schemas', Schemas);

SimpleSchema.messages({
  required: 'O campo [label] é obrigatório',
  'required imagem': 'O upload da imagem é obrigatório',
  'required documentacao.imagem': 'O upload da imagem é obrigatório',
  'required BO.imagem': 'O upload da imagem é obrigatório',
  'required logotipo': 'O upload do [label] é obrigatório',
  'required cnh.imagem': 'O upload da imagem é obrigatório',
  'required foto': 'O upload da foto é obrigatório'
});

Schemas.validatedMethodUpdateSchema = new SimpleSchema({
  _id: {
    type: String
  },
  modifier: {
    type: Object,
    blackbox: true
  }
});

Schemas.Endereco = new SimpleSchema({
  rua: {
    type: String
  },
  numero: {
    type: Number,
    label: 'Número'
  },
  complemento: {
    type: String,
    optional: true
  }
});

Schemas.Localizacao = new SimpleSchema({
  cep: {
    type: String,
    label: 'CEP',
    autoform: {
      type: 'cep'
    }
  },
  endereco: {
    type: Schemas.Endereco,
    label: 'Endereço'
  },
  bairro: {
    type: String
  },
  cidade: {
    type: String
  },
  estado: {
    type: String
  }
});

Schemas.CNH = new SimpleSchema({
  numero: {
    type: String,
    label: 'Número',
    autoform: {
      type: 'cnh'
    }
  },
  validade: {
    type: Date,
    optional: true,
    autoform: {
      type: 'bootstrap-datepicker',
      datePickerOptions: {
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      }
    }
  },
  imagem: {
    type: String,
    autoform: {
      type: 'upload'
    }
  }
});

Schemas.Contato = new SimpleSchema({
  telefone: {
    type: String,
    autoform: {
      type: 'intl-tel',
      class: 'form-control',
      intlTelOptions: {
        autoFormat: true,
        defaultCountry: 'BR',
      }
    }
  },
  celular: {
    type: String,
    optional: true,
    autoform: {
      type: 'intl-tel',
      class: 'form-control',
      intlTelOptions: {
        autoFormat: true,
        defaultCountry: 'BR',
      }
    }
  },
  email: {
    type: String,
    label: 'E-mail',
    regEx: SimpleSchema.RegEx.Email
  }
});

Schemas.CursoFeito = new SimpleSchema({
  identificacao: {
    type: String,
    label: 'Identificação'
  },
  data: {
    type: Date,
    optional: true,
    autoform: {
      type: 'bootstrap-datepicker',
      datePickerOptions: {
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      }
    }
  }
});

Schemas.TreinamentoFeito = new SimpleSchema({
  feito: {
    type: Boolean,
    autoform: {
      type: 'boolean-radios',
      trueLabel: 'Feito',
      falseLabel: 'Não Feito'
    }
  },
  curso: {
    type: Schemas.CursoFeito
  }
});

Schemas.BO = new SimpleSchema({
  numero: {
    type: String,
    label: 'Número'
  },
  imagem: {
    type: String,
    autoform: {
      type: 'upload'
    }
  }
});

Schemas.Condutor = new SimpleSchema({
  nome: {
    type: String
  },
  cnh: {
    type: String,
    label: 'CNH',
    autoform: {
      type: 'cnh'
    }
  },
  contato: {
    type: Schemas.Contato
  }
});

Schemas.Orgao = new SimpleSchema({
  identificacao: {
    type: String,
    label: 'Identificação'
  },
  codigo: {
    type: String,
    label: 'Código'
  }
});
