Empresas = new Mongo.Collection('empresas');

/**
 *
 * Schema
 *
 */
Schemas.Empresa = new SimpleSchema({
  razao: {
    type: String,
    label: 'Razão Social'
  },
  logotipo: {
    type: String,
    optional: true,
    autoform: {
      type: 'upload',
      collection: 'Images'
    }
  },
  localizacao: {
    type: Schemas.Localizacao,
    label: 'Localização'
  },
  cnpj: {
    type: String,
    label: 'CNPJ',
    autoform: {
      type: 'cnpj'
    }
  },
  inscricao: {
    type: Object,
    optional: true,
    label: 'Inscrição'
  },
  'inscricao.estadual': {
    type: String,
    optional: true
  },
  'inscricao.municipal': {
    type: String,
    optional: true
  },
  site: {
    type: String,
    optional: true
  },
  contato: {
    type: Schemas.Contato
  },
  obs: {
    type: String,
    label: 'Observação',
    optional: true,
    autoform: {
      rows: 6
    }
  }
});


/**
 *
 * Helpers
 *
 */
Empresas.helpers({

});

/**
 *
 * Behaviours
 *
 */
Empresas.attachSchema(Schemas.Empresa);
Empresas.attachBehaviour('timestampable');
