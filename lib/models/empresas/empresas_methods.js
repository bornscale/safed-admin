Empresas.methods = {};

Empresas.methods.add = new ValidatedMethod({
  name: 'Empresas.methods.add',
  validate: Schemas.Empresa.validator(),
  run(doc) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Empresas.insert(doc);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Empresas.methods.update = new ValidatedMethod({
  name: 'Empresas.methods.update',
  validate: Schemas.validatedMethodUpdateSchema.validator(),
  run({ _id, modifier }) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Empresas.update(_id, modifier);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Meteor.methods({
  'Empresas.methods.remove': function(empresaId) {
    check(empresaId, String);
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Empresas.remove({ _id: empresaId });
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});
