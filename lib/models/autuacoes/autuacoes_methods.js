Autuacoes.methods = {};

Autuacoes.methods.add = new ValidatedMethod({
  name: 'Autuacoes.methods.add',
  validate: Schemas.Autuacao.validator(),
  run(doc) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Autuacoes.insert(doc);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Autuacoes.methods.update = new ValidatedMethod({
  name: 'Autuacoes.methods.update',
  validate: Schemas.validatedMethodUpdateSchema.validator(),
  run({ _id, modifier }) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Autuacoes.update(_id, modifier);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Meteor.methods({
  'Autuacoes.methods.remove': function(autuacaoId) {
    check(autuacaoId, String);
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Autuacoes.remove({ _id: autuacaoId });
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});
