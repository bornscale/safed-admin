Autuacoes = new Mongo.Collection('autuacoes');

/**
 *
 * Schema
 *
 */
Schemas.Autuacao = new SimpleSchema({
  notificacao: {
    type: String,
    label: 'Notificação'
  },
  AIT: {
    type: String,
    label: 'AIT'
  },
  data: {
    type: Date,
    optional: true,
    autoform: {
      type: 'bootstrap-datepicker',
      datePickerOptions: {
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      }
    }
  },
  hora: {
    type: Date,
    optional: true,
    autoValue() {
      return new Date();
    },
    autoform: {
      omit: true
    }
  },
  infracao: {
    type: String,
    label: 'Infração'
  },
  enquadramento: {
    type: String
  },
  local: {
    type: String
  },
  orgao: {
    type: Schemas.Orgao,
    label: 'Orgão'
  },
  imagem: {
    type: String,
    autoform: {
      type: 'upload'
    }
  }
});


/**
 *
 * Helpers
 *
 */
Autuacoes.helpers({

});

/**
 *
 * Behaviours
 *
 */
Autuacoes.attachSchema(Schemas.Autuacao);
Autuacoes.attachBehaviour('timestampable');
