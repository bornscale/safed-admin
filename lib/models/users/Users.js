//                                       //
//                 SCHEMAS               //
//                                       //



//
// Profile
//
Schemas.Profile = new SimpleSchema({
  firstName: {
    type: String,
    label: 'Nome',
    optional: true
  },
  lastName: {
    type: String,
    label: 'Sobrenome',
    optional: true
  },
  fullName: {
    type: String,
    optional: true,
    autoValue: function() {
      let firstName = this.field('profile.firstName');
      let lastName = this.field('profile.lastName');
      if (firstName.isSet && lastName.isSet) {
        return firstName.value + ' ' + lastName.value;
      }
      return firstName.value;
    },
    autoform: {
      omit: true
    },
  },
  birth: {
    type: Date,
    optional: true,
    autoform: {
      type: 'bootstrap-datepicker',
      datePickerOptions: {
        format: 'yyyy/mm/dd',
        language: 'en-US'
      }
    }
  },
  localizacao: {
    type: Schemas.Localizacao,
    label: 'Localização',
    optional: true
  },
  contato: {
    type: Schemas.Contato,
    optional: true
  },
  gender: {
    type: String,
    optional: true,
    allowedValues: ['male', 'female'],
    autoform: {
      label: 'Gender',
      type: 'select-radio-inline',
      options: [{
        value: 'male',
        label: 'Male'
      }, {
        value: 'female',
        label: 'Female'
      }]
    }
  },
  imageId: {
    type: String,
    optional: true,
    label: 'Foto',
    autoform: {
      type: 'upload'
    }
  },
  rg: {
    type: String,
    optional: true,
    label: 'RG',
    autoform: {
      type: 'rg'
    }
  },
  cpf: {
    type: String,
    optional: true,
    label: 'CPF',
    autoform: {
      type: 'cpf'
    }
  }
});

//
// Invite
//
InviteSchema = new SimpleSchema({
  email: {
    type: String,
    autoform: {
      label: 'Email'
    },
    custom: function (){

    }
  },
  roles: {
    type: String,
    allowedValues: ['admin'],
    autoform: {
    label: 'Grupo',
      firstOption: 'Selecione um Grupo',
      type: 'selectize',
      options: [{
        value: 'admin',
        label: 'Admin'
      }]
    }
  },
  firstName: {
    type: String,
    label: 'Nome'
  },
  lastName: {
    type: String,
    label: 'Sobrenome'
  },
  cpf: {
    type: String,
    label: 'CPF',
    autoform: {
      type: 'cpf'
    }
  },
  rg: {
    type: String,
    label: 'RG',
    autoform: {
      type: 'rg'
    }
  },
  contato: {
    type: Schemas.Contato
  },
  localizacao: {
    type: Schemas.Localizacao,
    label: 'Localização'
  }
});

//OVERRIDE ERRORS MSGS
InviteSchema.messages({
  required: 'O campo [label] é obrigatório',
  'required roles': 'O campo Perfil é obrigatório'
});

Schemas.Email = new SimpleSchema({
  address: {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  verified: {
    type: Boolean
  }
});


Schemas.GlobalRoles = new SimpleSchema({
  __global_roles__: {
    type: [String],
  }
});

//
// Users
//
Schemas.Users = new SimpleSchema({
  username: {
    type: String,
    optional: true
  },
  emails: {
    type: [Schemas.Email],
    autoform: {
      omit: true
    },
  },
  profile: {
    type: Schemas.Profile,
    optional: true,
    blackbox: true,
    autoform: {
      omit: true
    }
  },
  services: {
    type: Object,
    optional: true,
    blackbox: true,
    autoform: {
      omit: true
    }
  },
  roles: {
    type: [String],
    optional: true,
    blackbox: true
  },
  status: {
    type: Object,
    blackbox: true,
    optional: true
  },
  active: {
    type: Boolean,
    autoValue() {
      return false;
    }
  },
});

Meteor.users.attachSchema(Schemas.Users);
Meteor.users.attachBehaviour('timestampable');


//                                       //
//                 HELPERS               //
//                                       //
Meteor.users.helpers({
  role() {
    let roles = Roles.getRolesForUser(this._id);
    return roles[0];
  },
  link() {
    return FlowRouter.path('usersEntry', {
      id: this._id
    });
  },
  identification(priority = ['fullName', 'username', 'email']) {
    let foundIdentification = 'unknown';
    _.some(priority, (p) => {
      switch (p) {
        case 'fullName':
          if (this.profile && this.profile.fullName) {
            foundIdentification = this.profile.fullName;
            return true;
          }
          return false;
        case 'username':
          if (this.username) {
            foundIdentification = this.username;
            return true;
          }
          return false;
        case 'email':
          if (this.emails && this.emails.length) {
            foundIdentification = this.emails[0].address;
            return true;
          }
          return false;
        default:
          return true;
      }
    });
    return foundIdentification;
  }
});


let pwdField = AccountsTemplates.removeField('password');
AccountsTemplates.addFields([{
  _id: 'firstName',
  type: 'text',
  required: true,
  displayName: 'Nome',
  placeholder: 'Nome'
}, {
  _id: 'lastName',
  type: 'text',
  required: true,
  displayName: 'Sobrenome',
  placeholder: 'Sobrenome'
}, pwdField]);
