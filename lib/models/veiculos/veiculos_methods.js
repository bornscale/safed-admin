Veiculos.methods = {};

Veiculos.methods.add = new ValidatedMethod({
  name: 'Veiculos.methods.add',
  validate: Schemas.Veiculo.validator(),
  run(doc) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Veiculos.insert(doc);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Veiculos.methods.update = new ValidatedMethod({
  name: 'Veiculos.methods.update',
  validate: Schemas.validatedMethodUpdateSchema.validator(),
  run({ _id, modifier }) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Veiculos.update(_id, modifier);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Meteor.methods({
  'Veiculos.methods.remove': function(veiculoId) {
    check(veiculoId, String);
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Veiculos.remove({ _id: veiculoId });
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});
