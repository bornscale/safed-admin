Veiculos = new Mongo.Collection('veiculos');

/**
 *
 * Schema
 *
 */
Schemas.Veiculo = new SimpleSchema({
  placa: {
    type: String,
    autoform: {
      type: 'placa'
    }
  },
  imagem: {
    type: String,
    optional: true,
    autoform: {
      type: 'upload'
    }
  },
  proprietario: {
    type: String,
    label: 'Proprietário'
  },
  marca: {
    type: String
  },
  modelo: {
    type: String
  },
  especie: {
    type: String,
    label: 'Espécie'
  },
  cor: {
    type: String
  },
  categoria: {
    type: String
  },
  ano: {
    type: Number
  },
  municipio: {
    type: String,
    label: 'Município'
  },
  chassi: {
    type: String
  },
  renavan: {
    type: String,
    label: 'RENAVAN'
  },
  documentacao: {
    type: Object,
    label: 'Documentação'
  },
  'documentacao.imagem': {
    type: String,
    autoform: {
      type: 'upload'
    }
  }
});


/**
 *
 * Helpers
 *
 */
Veiculos.helpers({

});

/**
 *
 * Behaviours
 *
 */
Veiculos.attachSchema(Schemas.Veiculo);
Veiculos.attachBehaviour('timestampable');
