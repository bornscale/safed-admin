Motoristas.methods = {};

Motoristas.methods.add = new ValidatedMethod({
  name: 'Motoristas.methods.add',
  validate: Schemas.Motorista.validator(),
  run(doc) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Motoristas.insert(doc);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Motoristas.methods.update = new ValidatedMethod({
  name: 'Motoristas.methods.update',
  validate: Schemas.validatedMethodUpdateSchema.validator(),
  run({ _id, modifier }) {
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Motoristas.update(_id, modifier);
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});

Meteor.methods({
  'Motoristas.methods.remove': function(motoristaId) {
    check(motoristaId, String);
    if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
      return Motoristas.remove({ _id: motoristaId });
    }
    throw new Meteor.Error(403, 'Not authorized');
  }
});
