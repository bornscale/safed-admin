Motoristas = new Mongo.Collection('motoristas');

/**
 *
 * Schema
 *
 */
Schemas.Motorista = new SimpleSchema({
  nome: {
    type: String,
  },
  nascimento: {
    type: Date,
    optional: true,
    autoform: {
      type: 'bootstrap-datepicker',
      datePickerOptions: {
        format: 'dd/mm/yyyy',
        language: 'pt-BR'
      }
    }
  },
  foto: {
    type: String,
    autoform: {
      type: 'upload'
    }
  },
  localizacao: {
    type: Schemas.Localizacao,
    label: 'Localização'
  },
  cpf: {
    type: String,
    label: 'CPF',
    autoform: {
      type: 'cpf'
    }
  },
  rg: {
    type: String,
    label: 'RG',
    autoform: {
      type: 'rg'
    }
  },
  cnh: {
    type: Schemas.CNH,
    label: 'CNH'
  },
  contato: {
    type: Schemas.Contato
  },
  treinamento: {
    type: Object,
    optional: true
  },
  'treinamento.teorico': {
    type: Schemas.TreinamentoFeito,
    label: 'Teórico',
    optional: true

  },
  'treinamento.pratico': {
    type: Schemas.TreinamentoFeito,
    label: 'Prático',
    optional: true
  },
  obs: {
    type: String,
    label: 'Observação',
    optional: true,
    autoform: {
      rows: 6
    }
  }
});


/**
 *
 * Helpers
 *
 */
Motoristas.helpers({

});

/**
 *
 * Behaviours
 *
 */
Motoristas.attachSchema(Schemas.Motorista);
Motoristas.attachBehaviour('timestampable');
