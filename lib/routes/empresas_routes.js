let empresasRoutes = FlowRouter.group({
  prefix: '/empresas',
  name: 'Empresas'
});

empresasRoutes.route('/', {
  name: 'empresas.dashboard',
  title: 'Empresas',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'empresasDashboard'
    });
  }
});


empresasRoutes.route('/add', {
  name: 'empresas.add',
  parent: 'empresas.dashboard',
  title: 'Nova Empresa',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'empresasAdd'
    });
  },
});

empresasRoutes.route('/:id/entry', {
  name: 'empresas.entry',
  parent: 'empresas.dashboard',
  title() {
    let organization = Empresas.findOne({
      _id: FlowRouter.getParam('id')
    });
    return organization && organization.name;
  },
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'empresasEntry'
    });
  },
});


empresasRoutes.route('/:id/edit', {
  name: 'empresas.edit',
  parent: 'empresas.entry',
  title: 'Editar Empresa',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'empresasEdit'
    });
  },
  subscriptions(params) {
    this.register('empresas', subs.subscribe('empresas.single', params.id));
  }
});
