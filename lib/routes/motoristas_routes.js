let motoristasRoutes = FlowRouter.group({
  prefix: '/motoristas',
  name: 'motoristas'
});

motoristasRoutes.route('/', {
  name: 'motoristas.dashboard',
  title: 'Motoristas',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'motoristasDashboard'
    });
  }
});

motoristasRoutes.route('/add', {
  name: 'motoristas.add',
  parent: 'motoristas.dashboard',
  title: 'Novo Motorista',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'motoristasAdd'
    });
  },
});

motoristasRoutes.route('/:id/entry', {
  name: 'motoristas.entry',
  parent: 'motoristas.dashboard',
  title() {
    let motorista = Motoristas.findOne({
      _id: FlowRouter.getParam('id')
    });
    return motorista && motorista.name;
  },
  subscriptions(params) {
    this.register('motoristas.single', subs.subscribe('motoristas.single', params.id));
  },
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'motoristasEntry'
    });
  },
});

motoristasRoutes.route('/:id/edit', {
  name: 'motoristas.edit',
  parent: 'motoristas.entry',
  title: 'Editar Motorista',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'motoristasEdit'
    });
  }
});
