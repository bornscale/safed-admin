let usersRoutes = FlowRouter.group({
  prefix: '/users',
});

usersRoutes.route('/', {
  name: 'users.dashboard',
  title: 'Usuários',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'usersDashboard'
    });
  },
});

usersRoutes.route('/invite', {
  name: 'users.invite',
  parent: 'users.dashboard',
  title: 'Convidar Usuário',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'usersInvite'
    });
  },
});

/*
usersRoutes.route('/:id/entry', {
  name: 'users.entry',
  parent: 'users.dashboard',
  title() {
    let user = Meteor.users.findOne({
      _id: FlowRouter.getParam('id')
    });
    return user && user.identification();
  },
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'usersEntry'
    });
  }
});
*/

usersRoutes.route('/:id/edit', {
  name: 'users.edit',
  parent: 'users.dashboard',
  title: 'Editar Usuário',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'usersEdit'
    });
  },
  subscriptions(params) {
    this.register('user', subs.subscribe('singleUser', params.id));
  }
});
