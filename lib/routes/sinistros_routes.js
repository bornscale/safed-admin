let sinistrosRoutes = FlowRouter.group({
  prefix: '/sinistros',
  name: 'Sinistros'
});

sinistrosRoutes.route('/', {
  name: 'sinistros.dashboard',
  title: 'Gerenciar Sinistros',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'sinistrosDashboard'
    });
  }
});


sinistrosRoutes.route('/add', {
  name: 'sinistros.add',
  parent: 'sinistros.dashboard',
  title: 'Criar Sinistro',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'sinistrosAdd'
    });
  },
});

sinistrosRoutes.route('/:id/entry', {
  name: 'sinistros.entry',
  parent: 'sinistros.dashboard',
  title() {
    let sinistro = Sinistros.findOne({
      _id: FlowRouter.getParam('id')
    });
    return sinistro && sinistro.name;
  },
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'sinistrosEntry'
    });
  },
});


sinistrosRoutes.route('/:id/edit', {
  name: 'sinistros.edit',
  parent: 'sinistros.entry',
  title: 'Editar Sinistro',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'sinistrosEdit'
    });
  }
});
