let veiculosRoutes = FlowRouter.group({
  prefix: '/veiculos',
  name: 'Veículos'
});

veiculosRoutes.route('/', {
  name: 'veiculos.dashboard',
  title: 'Gerenciar Veículos',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'veiculosDashboard'
    });
  }
});


veiculosRoutes.route('/add', {
  name: 'veiculos.add',
  parent: 'veiculos.dashboard',
  title: 'Criar Veículo',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'veiculosAdd'
    });
  },
});

veiculosRoutes.route('/:id/entry', {
  name: 'veiculos.entry',
  parent: 'veiculos.dashboard',
  title() {
    let veiculo = Veiculos.findOne({
      _id: FlowRouter.getParam('id')
    });
    return veiculo && veiculo.name;
  },
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'veiculosEntry'
    });
  },
});


veiculosRoutes.route('/:id/edit', {
  name: 'veiculos.edit',
  parent: 'veiculos.entry',
  title: 'Editar Veículo',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'veiculosEdit'
    });
  }
});
