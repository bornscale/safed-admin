let autuacoesRoutes = FlowRouter.group({
  prefix: '/autuacoes',
  name: 'Autuações'
});

autuacoesRoutes.route('/', {
  name: 'autuacoes.dashboard',
  title: 'Gerenciar Autuações',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'autuacoesDashboard'
    });
  }
});


autuacoesRoutes.route('/add', {
  name: 'autuacoes.add',
  parent: 'autuacoes.dashboard',
  title: 'Criar Autuação',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'autuacoesAdd'
    });
  },
});

autuacoesRoutes.route('/:id/entry', {
  name: 'autuacoes.entry',
  parent: 'autuacoes.dashboard',
  title() {
    let autuacao = Autuacoes.findOne({
      _id: FlowRouter.getParam('id')
    });
    return autuacao && autuacao.name;
  },
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'autuacoesEntry'
    });
  },
});


autuacoesRoutes.route('/:id/edit', {
  name: 'autuacoes.edit',
  parent: 'autuacoes.entry',
  title: 'Editar Autuação',
  action() {
    BlazeLayout.render('defaultLayout', {
      main: 'autuacoesEdit'
    });
  }
});
